﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TestTaskWebApp.Services
{
    public class ImagesService
    {
        private IMemoryCache cache;

        public ImagesService(IMemoryCache memoryCache)
        {
            cache = memoryCache;
        }
                
        public byte[] GetImage(string path)
        {
            try
            {
                byte[] file = null;
                string fileName = Path.GetFileName(path);

                if (!cache.TryGetValue(fileName, out file))
                {
                    file = File.ReadAllBytes(path);
                    cache.Set(fileName, file,
                        new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(1)));
                }
                return file;
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }

        public async void AddImage(IFormFile file, string directoryPath)
        {
            string path = Path.Combine(directoryPath, file.FileName);
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            // Read file into byte array to add to cache
            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.OpenReadStream()))
            {
                fileData = binaryReader.ReadBytes((int)file.Length);
            }
            
            cache.Set(file.FileName, fileData,
                new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(1)));
        }


    }
}
