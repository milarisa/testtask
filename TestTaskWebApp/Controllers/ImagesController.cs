﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TestTaskWebApp.Services;

namespace TestTaskWebApp.Controllers
{
    [Route("api/[controller]")]
    public class ImagesController : Controller
    {
        DirectorySettings dirSettings;
        ImagesService imageService;

        public ImagesController(IOptions<DirectorySettings> directorySettings, ImagesService service)
        {
            dirSettings = directorySettings.Value;
            imageService = service;
        }
               
        // GET api/images/name.jpg

        [HttpGet("{nameFile}")]
        public IActionResult GetImage(string nameFile)
        {
            string filePath = Path.Combine(dirSettings.DirectoryName, nameFile);
            byte[] image = imageService.GetImage(filePath);

            if (image == null)
            {
                return NotFound();
            }

            string extention = Path.GetExtension(filePath).Substring(1);
            string contentType = "image/" + extention;
            return File(image, contentType, nameFile);
        }
        
        // POST api/images

        [HttpPost]
        public IActionResult AddImage(IFormFile file)
        {
            if (file == null)
            {
                return BadRequest();
            }

            imageService.AddImage(file, dirSettings.DirectoryName);
            return StatusCode(201);
        }


    }
}